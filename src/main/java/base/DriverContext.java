package base;

import org.openqa.selenium.WebDriver;

public class DriverContext {

    public static WebDriver _driver;

    public static Browser _browser;

    public static void setDriver(WebDriver driver){
        _driver = driver;
    }
}
