package base;

import org.openqa.selenium.WebDriver;

public class Browser {
    private WebDriver _driver;
    public BrowserType type;

    public Browser(WebDriver driver){
        _driver = driver;
    }

    public void goToUrl(String url){
        _driver.get(url);
    }

    public void maximize(){
        _driver.manage().window().maximize();
    }
}
