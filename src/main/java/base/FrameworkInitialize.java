package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FrameworkInitialize extends Base{

    public void initializeBrowser(BrowserType browserType){

        WebDriver driver = null;

        switch (browserType){

            case Chrome:
                System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
                driver = new ChromeDriver();
                break;

            case Firefox:
                driver = new FirefoxDriver();
                break;

            case IE:
                break;
        }

        // Set the driver
        DriverContext.setDriver(driver);

        // Browser
        DriverContext._browser = new Browser(driver);

    }
}
