package steps;

import base.BrowserType;
import base.DriverContext;
import base.FrameworkInitialize;
import cucumber.api.java.Before;

public class TestInitialize extends FrameworkInitialize {
    @Before
    public void initialize(){
        initializeBrowser(BrowserType.Chrome);
        DriverContext._browser.goToUrl("http://www.executeautomation.com/demosite/Login.html");
    }
}
