package pages;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePage extends BasePage {
    public HomePage(){

    }

    @FindBy(how = How.LINK_TEXT, using = "Login")
    public WebElement lnkLogin;

    public LoginPage clickLogin(){
        // Click login button on homepage to open Login page.
        lnkLogin.click();
        return GetInstance(LoginPage.class);
    }

    public boolean isLogin(){
        return true;
    }
}
