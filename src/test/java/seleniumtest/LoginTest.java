package seleniumtest;

import base.BrowserType;
import base.DriverContext;
import base.FrameworkInitialize;
import org.junit.Before;
import org.junit.Test;
import pages.HomePage;
import pages.LoginPage;
import utilities.LogUtil;

public class LoginTest extends FrameworkInitialize {

    @Before
    public void initialize(){
        initializeBrowser(BrowserType.Chrome);
        DriverContext._browser.goToUrl("http://www.executeautomation.com/demosite/Login.html");
    }

    @Test
    public void login(){
        //currentPage = GetInstance(HomePage.class);

        // Click login button on homepage to open Login page.
        //currentPage = currentPage.As(HomePage.class).clickLogin();
        currentPage = GetInstance(LoginPage.class);

        currentPage.As(LoginPage.class).login("admin", "admin");
        currentPage.As(LoginPage.class).clickLogin();
    }
}
