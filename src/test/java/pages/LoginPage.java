package pages;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends BasePage {

    public LoginPage(){
        super();
    }

    @FindBy(how = How.NAME, using = "UserName")
    public WebElement txtUserName;

    @FindBy(how = How.NAME, using = "Password")
    public WebElement txtPassword;

    @FindBy(how = How.NAME, using = "Login")
    public WebElement btnLogin;


    public void login(String userName, String password){
        txtUserName.sendKeys(userName);
        txtPassword.sendKeys(password);

    }

    public HomePage clickLogin(){
        btnLogin.submit();
        return GetInstance(HomePage.class);
    }

}
