package steps;

import base.Base;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.HomePage;
import pages.LoginPage;
import utilities.CucumberUtil;

import java.util.List;

public class LoginSteps extends Base {
    @Given("^I have navigated to the application$")
    public void iHaveNavigatedToTheApplication() {
        // Write code here that turns the phrase above into concrete actions
    }

    @And("^I see application opened$")
    public void iSeeApplicationOpened() throws Throwable {
        currentPage = GetInstance(HomePage.class);
        Assert.assertTrue("The login page is not loaded", currentPage.As(HomePage.class).isLogin());
    }

    @Then("^I click login link$")
    public void iClickLoginLink() throws Throwable {
        currentPage = GetInstance(LoginPage.class);
    }

    @When("^I enter Username and Password$")
    public void iEnterUsernameAndPassword(DataTable data) throws Throwable {
        //List<List<String>> table = data.raw();
        //currentPage.As(LoginPage.class).login(table.get(1).get(0).toString(), table.get(1).get(1).toString());
        CucumberUtil.convertDataTableToDict(data);
        currentPage.As(LoginPage.class).login(CucumberUtil.getCellValue("Username"), CucumberUtil.getCellValue("Password"));
    }

    @Then("^I click login button$")
    public void iClickLoginButton() throws Throwable {
        currentPage = currentPage.As(LoginPage.class).clickLogin();
    }

    @Then("^I should see the username with hello$")
    public void iShouldSeeTheUsernameWithHello() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    }
}
